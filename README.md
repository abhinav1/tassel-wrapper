1. retrieve all meta data in the hdf5 file, and write them into a file

Usage:  java -jar Tassel_gt_server.jar dbinfo [options]

Options: 
                -sf     source file name
                -st     source file type (hdf5, vcf, or hapmap)
                -dbname database name in one word
                -o      output directory
Output:
new tab delimited text file:
*.chrinfo: 
""  + "\t" + dbname + "\t" + chromsome + "\t" +	length	+ "\t" + siteCount

*.dbinfo:  
""  + "\t" + dbname + "\t" + description + "\t" + genomeversion + "\t" + source_file + "\t" + source_file_type + "\t" + date_created + "\t" + hasdepth + "\t" + tassel_version + "\n" 


*.taxainfo:
dbname + "\t" + taxaName + "\t" + Annotation_key + "\t" + Annotation_value + "\n"

example:
mkdir dbinfo
java -jar ~/hdf5/tassel-gt-server/tassel-gt-server.jar dbinfo -sf /local/ZeaGBS/genos/built/AllZeaGBSv27taxaAnnotated20140528.h5 -st hdf5 -dbname AllZeaGBSv27taxaAnnotated20140528 -o dbinfo 






2. query the genotype matrix
Usage:  java -jar Tassel_gt_server.jar slice [options]

Options		-bf     build the file. If skip, only give the dimensions
                -sf     source file name
                -st     source file type (hdf5, vcf, or hapmap)
                -df     destination file name
                -dt     destination file type (hdf5, vcf, hapmap or plink)
                -tf     taxa file name
                -ch     chromosome name
                -start  physical start position on the chromosome
                -end    physical end position on the chromosome
Example:
make a hdf5 with a subset of the genotyping data
java -jar ~/hdf5/tassel-gt-server/tassel-gt-server.jar slice -bf -sf /local/ZeaGBS/genos/built/AllZeaGBSv27taxaAnnotated20140528.h5 -st hdf5 -df public.hdf5 -dt hdf5 -tf dbinfo/publiclist